jQuery(document).ready(function($) {
    $('#config_company').click(function() {
        var abs_url = api_script_object.ajaxurl
        $(this).nextAll().remove();
        $.ajax({
            type: "POST",
            url: abs_url,
            data: {
                'action': 'api_service_config_company'
            },
            success: function(data){
                data = data.split('Array').join('');
                var obj = JSON.parse(data);
                if (obj.success == true) {
                    var strong = JSON.stringify(obj, undefined, 4);
                    $('#facturaloperu_api_config_response').val(strong);
                    $('#facturaloperu_api_config_token').val(obj.token);
                } else {
                    console.log(obj);
                }
            }
        });
    });
});