<?php

function add_scripts_api_resolution() {
    wp_enqueue_script(
        'api-resolution',
        plugin_dir_url( __FILE__ ) . '../../js/api-resolution.js',
        array( 'jquery' )
    );

    wp_localize_script('api-resolution', 'api_script', array(
        'ajaxurl' => admin_url('admin-ajax.php')
    ));
}

add_action('admin_enqueue_scripts', 'add_scripts_api_resolution');
add_action( 'wp_ajax_api_service_config_resolution', 'api_service_config_resolution' );
add_action( 'wp_ajax_nopriv_api_service_config_resolution', 'api_service_config_resolution' );

function api_service_config_resolution() {
    $type = $_POST['type'];

    $api_url = get_option('facturaloperu_api_config_url');
    $api_token = 'Bearer '.get_option('facturaloperu_api_config_token');

    $scheme = parse_url($api_url, PHP_URL_SCHEME) != '' ? parse_url($api_url, PHP_URL_SCHEME) : 'http';
    $service_url = $scheme.'://'.parse_url($api_url, PHP_URL_HOST).'/api/ubl2.1/config/resolution';

    $json_body = wp_json_encode([
        'type_document_id' => get_option('facturaloperu_api_resolution_document_type'),
        'resolution' => get_option('facturaloperu_api_resolution'),
        'prefix' => get_option('facturaloperu_api_resolution_prefix'),
        'resolution_date' => get_option('facturaloperu_api_resolution_date'),
        'technical_key' => get_option('facturaloperu_api_resolution_technical_key'),
        'from' => get_option('facturaloperu_api_resolution_number_from'),
        'to' => get_option('facturaloperu_api_resolution_number_to'),
        'generated_to_date' => get_option('facturaloperu_api_resolution_generated_date'),
        'date_from' => get_option('facturaloperu_api_resolution_date_start'),
        'date_to' => get_option('facturaloperu_api_resolution_date_stop')
    ]);

    // ENVIO A LA API
    $json_response = wp_remote_request( $service_url, array(
        'method' => 'PUT',
        'headers' => array(
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => $api_token
        ),
        'sslverify' => false,
        'body' => $json_body
    ));

    if ( is_wp_error( $json_response ) ) {
        $error_message = $json_response->get_error_message();
        echo "Something went wrong: $error_message";
    } else {
        echo $json_response;
        $json = wp_remote_retrieve_body( $json_response );
        echo $json;
    }

    wp_die();
}