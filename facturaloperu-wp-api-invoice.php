<?php
/**
 * Plugin Name: co-wp-api
 * Plugin URI: http://facturalatam.com
 * Description: Envio de Facturas a la DIAN
 * Version: 1.0.1
 * Author: Facturalatam
 * Author URI: http://facturalatam.com
 * Requires at least: 4.0
 * Tested up to: 5.0.1
 *
 * Text Domain: co-wp-api
 * Domain Path: /languages/
 */


defined( 'ABSPATH' ) or die( '¡Sin trampas!' );

/*
 * functions.php
 *
 */
require_once( __DIR__ . '/includes/json-generate.php');
require_once( __DIR__ . '/includes/send-invoice.php');
// require_once( __DIR__ . '/includes/send-option.php');
require_once( __DIR__ . '/includes/front/fields.php');
require_once( __DIR__ . '/includes/admin/api-config.php');
// require_once( __DIR__ . '/includes/query-document.php');
require_once( __DIR__ . '/includes/admin/send/api-company.php');
require_once( __DIR__ . '/includes/admin/send/api-software.php');
require_once( __DIR__ . '/includes/admin/send/api-certificate.php');
require_once( __DIR__ . '/includes/admin/send/api-resolution.php');
require_once( __DIR__ . '/includes/admin/send/api-initial.php');
